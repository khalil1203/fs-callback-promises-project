const fs = require('fs');
const answer2 = function (lipsum) {

    const readData = readingFile(lipsum);

    readData.then((data) => {
        console.log(data);
        return writingFile(data.toUpperCase(), 'upperCase.txt')
    })
        .then((data) => {
            console.log(data);
            return appendingFile('upperCase.txt' + '\n');
        })
        .then((data) => {
            console.log(data);
            return writingFile(data.toLocaleLowerCase()
                .split('. ')
                .join('\n'), './lowerCase.txt');
        })
        .then((data) => {
            console.log(data);
            return appendingFile('lowerCase.txt' + '\n');
        })
        .then((data) => {
            console.log("reading the file");
            return readingFile('./lowerCase.txt');
        })
        .then((data) => {
            console.log("writing sorted data");
            return writingFile(data.split('\n')
                .sort()
                .join('\n'), './sortedData.txt');
        })
        .then((data) => {
            console.log("appending succesfull!!!!");
            return appendingFile('sortedData.txt');
        })
        .then((data) => {
            console.log("reading file!!!!");
            return readingFile('./filenames.txt');
        })
        .then((data) => {
            console.log("deleting files!!!!");
            return deletingFile(data);
        })
        .catch((err)=>{
            console.log(err);
        })

    function readingFile(lipsum) {
        return new Promise((reslove, reject) => {
            fs.readFile(lipsum, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    reslove(data);
                }
            });
        })
    }


    function writingFile(data, filePath) {
        return new Promise((reslove, reject) => {

            fs.writeFile(filePath, data, (err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    reslove("file is created and write");
                }
            });

        });
    }


    function appendingFile(filePath) {
        return new Promise((reslove, reject) => {
            fs.appendFile('filenames.txt', filePath, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    reslove("file is append");
                }
            });
        });
    }


    function deletingFile(data) {
        new Promise((resolve, reject) => {
            data = data.split('\n');
            let counter = 0;
            console.log(data.length)
            data.map((file) => {
                counter++;
                fs.unlink(`${file}`, (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        if (counter == data.length) {
                            resolve("all files are deleted!!!!");
                        }
                    }
                });
            });
        })
            .then((data) => {
                console.log(data);

                fs.unlink('filenames.txt', (err) => {
                    if (err) {
                        console.log(err);
                    } else {

                        console.log("folder succesful deleted");
                    }
                });
            })
    }
}




module.exports = answer2;