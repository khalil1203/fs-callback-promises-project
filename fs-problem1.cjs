const fs = require('fs');


// Creating function to create and delete .json files simultaneously!!!
const create_delete_json = (directory, random_num) => {
    let count = 0;
    //Creating a directory using fs module!!
    function invoked() {
        return new Promise((resolve, reject) => {
            fs.mkdir(directory, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve("Directory is created");
                }
            });
        });
    }




    invoked()
        .then((data) => {
            console.log(data);
            new Promise((resolve, reject) => {
                for (let index = 0; index < random_num; index++) {

                    let data = JSON.stringify({
                        id: index,
                        name: `random${index}`
                    });

                    // Creating new file using fs module in the directory!!!
                    fs.writeFile(`${directory}/random${index}.json`, data, (err) => {
                        if (err) {
                            reject(err);
                        }
                        else {
                            count++;
                            console.log(`File created : random${index}.json`);
                            if (count >= random_num) {
                                resolve("files created successfully");
                            }

                        }
                    });

                }
            })
                .then((data) => {
                    console.log(data);

                    for (let index = 0; index < random_num; index++) {
                        const filepath = `${directory}/random${[index]}.json`;

                        //Deleting the file!!!
                        fs.unlink(filepath, (err) => {
                            if (err) {
                                console.error(`Error deleting file ${index}: ${err}`);

                            }
                            else {
                                console.log(`File deleted: ${filepath}`);
                            }
                        })
                    }

                    fs.rmdir(directory, (err) => {

                        if (err) {
                            console.log("error occurred in deleting directory", err);
                        }

                        console.log("Directory deleted successfully");
                    });
                })
                .catch((err)=>{
                    console.log(err);
                });


        });






}


module.exports = create_delete_json;


